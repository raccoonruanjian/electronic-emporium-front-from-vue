module.exports = {
    devServer: {
        proxy: {
            "/api": {
                target: "http://localhost:8080",
                changeOrigin: true,
                pathRewrite: {
                    "^/api": ""
                }
            },
            
        },
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        hotOnly: false,
        disableHostCheck: true,
    }
}