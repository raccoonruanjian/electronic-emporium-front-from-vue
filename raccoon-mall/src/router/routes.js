import index from '../components/index'

let routes = [{
    path: '/',
    component: index,

},
{
    path: '/brandPage',
    name: 'brandPage',
    title: "品牌页",
    component: () => import('../views/brandPage')
},
{
    path: '/about',
    name: 'about',
    title: "关于我们",
    component: () => import('../views/about')
},

{
    path: '/brandSingleDetail',
    name: 'brandSingleDetail',
    title: "品牌详情",
    component: () => import('../views/brandDetail')
},
{
    path: '/productSingleContainer',
    name: 'productSingleContainer',
    title: "分类页",
    component: () => import('../views/productPage')
},
{
    path: '/productDetail',
    name: 'productDetail',
    title: "商品详情",
    component: () => import('../views/productDetail')
},
{

    path: '/shoppingDetail',
    name: 'shoppingDetail',
    title: "购物车",
    component: () => import('../views/shoppingDetail')
},
{
    path: '/login',
    name: 'login',
    title: "登录",
    component: () => import('../views/login')
},
{
    path: '/register',
    name: 'register',
    title: "注册",
    component: () => import('../views/register')
},
{
    path: '/forgetpassword',
    name: 'forgetpassword',
    title: "忘记密码",
    component: () => import('../views/forgetpassword')
},
{
    path: '/matterAnswer',
    name: 'matterAnswer',
    title: "密保答案",
    component: () => import('../components/matterAnswer')
},
{
    path: '/resetPassword',
    name: 'resetPassword',
    title: "重置密码",
    component: () => import('../components/resetPassword')
},
{
    path: '/paySuccess',
    name: 'paySuccess',
    title: "支付成功",
    component: () => import('../components/paySuccess')
},
]

export default routes