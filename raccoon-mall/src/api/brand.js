import request from "../utils/request"

// 获取品牌信息
export function getBrandList() {
    return request.get("/General/getBrandInfo")
}

// 通过ID获取品牌信息
export function getBrandDetail(id){
    return request.get(`/BrandInfo/getProduct/${id}`)
}