import request from "../utils/request"

// 获取密保问题
export function getProblem() {
    return request.get("/general/problem")
}

export function registerBase(data) {
    return request.post("/user/closing", data)
}
export function registerInfo(id, data) {
    return request.post(`/user/newUserInfo/${id}`, data)
}

export function login(data) {
    return request.post("/user/login", data)
}

// 获取个人信息
export function personalInfo(id) {
    return request.get(`/user/getuserinof/${id}`)
}


// 找回密码第一步
export function forgerPassOne(data) {
    return request.get(`/user/getuserid/${data}`)
}

// 找回密码第二步，通过id获取密保
export function getPensonalProblem(id) {
    return request.get(`/user/getuserproblem/${id}`)
}

// 
export function getCurrentAnswer(id, parmas) {
    return request.put(`/user/validanswer/${id}`, parmas)
}
export function updatePassword(id, data) {
    return request.put(`/user/uppassword/${id}`, data)
}

// 修改个人信息
export function updateUserInfo(id,data){
    return request.put(`/user/upuserinof/${id}`,data)
}

// 获取当前IP位置
export function getIpAddress() {
    return request.get("https://restapi.amap.com/v3/ip?parameters&key=241b11c51787c21517d2e95e8f309075")
}

// 获取当前用户收货地址
export function getUserAddress(id) {
    return request.get(`/General/getuseraddress/${id}`)
}

// 添加用户收货地址
export function addAddress(id, data) {
    return request.post(`/BackUserAddress/addUserAddress/${id}`, data)
}

// 用户修改收货地址
export function updateUserAddress(id, data) {
    return request.put(`/BackUserAddress/upUserAddress/${id}`, data)
}

// 删除收货地址
export function deleteUserAddress(id) {
    return request.delete(`/BackUserAddress/delUserAddress/${id}`)
}

// 用户提交订单
export function submitOrder(id, data) {
    return request.post(`/Order/addorder/${id}`, data)
}

// 用户获取订单
export function getOrder(id){
    return request.get(`/Order/getuserOrder/${id}`)
}

// 获取订单详情
export function getOrderDetail(id){
    return request.get(`/Order/getuserOrderinfo/${id}`)
}