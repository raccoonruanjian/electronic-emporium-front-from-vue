import request from "../utils/request"

// 获取分类
export function getCategory(){
    return request.get("/Category/getCategory")
}

// 根据分类id
export function getProductCategory(id){
    return request.get(`/Category/getProduct/${id}`)
}